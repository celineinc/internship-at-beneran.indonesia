from flask import Flask
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)

users = [
    {"identifier": "Baba",
     "age": 20,
     "occupation":"SE"
     },
    {"identifier": "Bebe",
     "age": 25,
     "occupation":"Admin"
    }
]

class UserList(Resource):
    def get(self):
            if (len(users) !=0 ):
                return users, 200
            return "There is no user", 404

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("identifier", required=True)
        parser.add_argument("age", required=True)
        parser.add_argument("occupation", required=True)
        # Parse the arguments into an object
        args = parser.parse_args()

        for user in users:
            if(args["identifier"] == user["identifier"]):
                return "User with name {} already exists.".format(args["identifier"]), 400

        user = {
            "identifier": args["identifier"],
            "age": args["age"],
            "occupation": args["occupation"]
            }
        users.append(user)
        return "Succesfully added", 201

    def delete(self):
        del(users[0:])
        return "All users are deleted", 200

class User(Resource):
    def get(self, name):
        for user in users:
            if(name == user["identifier"]):
                return user, 200
        return "User not found", 404

    def post(self,name):
        parser = reqparse.RequestParser()
        parser.add_argument("age", required=True)
        parser.add_argument("occupation", required=True)
        # Parse the arguments into an object
        args = parser.parse_args()

        for user in users:
            if(name == user["identifier"]):
                return "User with name {} already exists.".format(name), 400

        user = {
            "identifier": name,
            "age": args["age"],
            "occupation": args["occupation"]
            }
        users.append(user)
        return "Succesfully added", 201

    def delete(self,name):
        for user in users:
            if(name == user["identifier"]):
                users.remove(user)
                return "{} is deleted.".format(name), 200
        return "User with name {} doesn't exist.".format(name), 404

api.add_resource(UserList, "/user")
api.add_resource(User, "/user/<string:name>")
app.run(debug=True)