from flask import Flask, jsonify, request
from flask_restful import Api, Resource, reqparse


app = Flask(__name__)
app.config["DEBUG"] = True
api = Api(app)
indonesia = [
    {"Provinsi": "DKI Jakarta",
    "Ibu Kota": "Jakarta",
    "Bagian Waktu": "WIB"},    
    {"Provinsi": "Jawa Barat",
    "Ibu Kota": "Bandung",
    "Bagian Waktu": "WIB"},  
    {"Provinsi": "Bali",
    "Ibu Kota": "Bali",
    "Bagian Waktu": "WITA"}
]

@app.route('/', methods=['GET'])
def home():
    return "<h1> Daftar API Provinsi di Indonesia <h1>"

@app.route('/provinsi/indonesia', methods=['GET'])
def api_all():
    return jsonify(indonesia)

@app.route('/provinsi', methods=['GET'])
def api_namaprovinsi():
    results = []

    if 'Provinsi' in request.args:
        for provinsi in indonesia:
                if(provinsi["Provinsi"] == request.args['Provinsi']):
                    results.append(provinsi)
    else:
        return "Provinsi yang anda inginkan tidak ada"

    return jsonify(results)

class Provinsi(Resource):
    def get(self, namaProvinsi):
        for provinsi in indonesia:
            if(namaProvinsi == provinsi["Provinsi"]):
                return provinsi, 200
        return "Provinsi tersebut tidak ada di Indonesia", 404

    def post(self, namaProvinsi):

        parser = reqparse.RequestParser()
        parser.add_argument("Ibu Kota")
        parser.add_argument("Bagian Waktu")
        args = parser.parse_args()

        for provinsi in indonesia:
            if(namaProvinsi == provinsi["Provinsi"]):
                return "Provinsi {} tersebut sudah masuk ke dalam data".format(namaProvinsi), 400

        provinsi = {
            "Provinsi" : namaProvinsi,
            "Ibu Kota" : args["Ibu Kota"],
            "Bagian Waktu" : args["Bagian Wanktu"]
        }

        indonesia.append(provinsi)
        return provinsi, 201

    def delete(self, namaProvinsi):
        global indonesia
        indonesia = [provinsi for provinsi in indonesia if provinsi["Provinsi"] != namaProvinsi]
        return "{} is deleted.".format(namaProvinsi), 200

class Indonesia(Resource):
    def delete(self):
        del(indonesia[0:])
        return "Semua provinsi telah di hapus", 200

api.add_resource(Provinsi, "/provinsi/<string:namaProvinsi>")
api.add_resource(Indonesia, "/provinsi")
app.run()
